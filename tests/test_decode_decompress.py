import pytest

from pareqs.pareqs import base64_decode, decode_pareqs


@pytest.mark.parametrize(['de', 'en'], [
    ("В моих голосах: какая-то ложь", "0JIg0LzQvtC40YUg0LPQvtC70L7RgdCw0YU6INC60LDQutCw0Y8t0YLQviDQu9C+0LbRjA=="),
    ("Какая-то фальшь", "0JrQsNC60LDRjy3RgtC+INGE0LDQu9GM0YjRjA=="),
    ("Такие тона в твоих волосах", "0KLQsNC60LjQtSDRgtC+0L3QsCDQsiDRgtCy0L7QuNGFINCy0L7Qu9C+0YHQsNGF"),
    ("Искусственный дождь", "0JjRgdC60YPRgdGB0YLQstC10L3QvdGL0Lkg0LTQvtC20LTRjA=="),
    ("Искусственный снег", "0JjRgdC60YPRgdGB0YLQstC10L3QvdGL0Lkg0YHQvdC10LM="),
    ("И кажется хна", "0Jgg0LrQsNC20LXRgtGB0Y8g0YXQvdCw"),
])
def test_base_64decoding(de, en):
    assert base64_decode(en).decode() == de


@pytest.mark.parametrize(['de', 'en'], [
    ("Я хочу повеситься", "eNoBIADf/9CvINGF0L7Rh9GDINC/0L7QstC10YHQuNGC0YzRgdGPYJgVsA=="),
    ("Фонарь", "eNq7sOTCvgt7L2y42HCxBwA6kwi+"),
    ("Верёвка", "eNq7MOnC1osNFyde2HRh14UNAEugCic="),
    ("Лестница", "eNq7MPvC1ouNF5su7L2w42LbhQ0AYhILgg==")
])
def test_decompress_base64(de, en):
    assert decode_pareqs(en) == de
